const { defineConfig } = require('@vue/cli-service')
const ModuleFederationPlugin = require('webpack').container.ModuleFederationPlugin
const deps = require('./package.json').dependencies
const path = require('path')

module.exports = defineConfig({
  transpileDependencies: true,
  publicPath: 'http://localhost:8080/',
  configureWebpack: {
    optimization: {
      splitChunks: false
    },
    plugins: [
      new ModuleFederationPlugin({
        name: 'consumer',
        filename: 'remoteEntry.js',
        exposes: {},
        remotes: {
          header: 'header@http://localhost:8081/remoteEntry.js'
        },
        shared: {
          ...deps,
          vue: {
            eager: true,
            singleton: true,
            requiredVersion: deps.vue,
            strictVersion: true
          }
        }
      })
    ]
  },
  devServer: {
    compress: true,
    headers: {
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE, PATCH, OPTIONS",
      "Access-Control-Allow-Headers": "X-Requested-With, content-type, Authorization",
    },
    hot: false,
    liveReload: true,
    port: 8080,
    static: {
      directory: path.join(__dirname, "public")
    }
  }
})
